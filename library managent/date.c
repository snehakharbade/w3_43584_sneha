#include<stdio.h>
#include"time.h"
#include"date.h"

void date_accept(date_t *d) {
    printf("date: ");
    scanf("%d %d %d", &d->day, &d->month, &d->year);
}

void date_print(date_t *d) {
    printf("%d %d %d", d->day, d->month, d->year);
}
